package gopushbullet

func (this *Client) PushTo(target TargetType, push PushType) error {
	return this.httpPost("/pushes", union(target, push))
}
