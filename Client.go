package gopushbullet

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

const baseUrl = "https://api.pushbullet.com/api"

type Client struct {
	authHeaderValue string
	httpClient *http.Client 
}

func NewClient(accessToken string) *Client {
	this := new(Client)
	this.authHeaderValue = "Basic " + base64.StdEncoding.EncodeToString([]byte(accessToken+":"))
	this.httpClient = http.DefaultClient
	return this
}

func (this *Client) httpPost(relativeUrl string, data requestData) error {
	var dataBuffer bytes.Buffer
	encoder := json.NewEncoder(&dataBuffer)
	encoder.Encode(data.GetData())

	httpRequest, err := http.NewRequest("POST", baseUrl + relativeUrl, ioutil.NopCloser(&dataBuffer))
	if err != nil {
		panic(err)
	}

	httpRequest.Header.Set("Authorization", this.authHeaderValue)
	httpRequest.Header.Set("Content-Type", "application/json")

	httpResponse, err := this.httpClient.Do(httpRequest)
	if err != nil {
		return err
	}
	defer httpResponse.Body.Close()

	if httpResponse.StatusCode != http.StatusOK {
		return errors.New(httpResponse.Status)
	}
	return nil
}
