package gopushbullet

type PushType interface {
	requestData
	GetPushTypeName() string
}

type Note struct {
	Title string
	Body string
}
func (this Note) GetPushTypeName() string { return "Note" }
func (this Note) GetData() map[string]interface{} {
	out := make(map[string]interface{})
	out["type"] = "note"
	out["title"] = this.Title
	out["body"] = this.Body
	return out
}

type Link struct {
	Title string
	Url string
	Body string
}
func (this Link) GetPushTypeName() string { return "Link" }
func (this Link) GetData() map[string]interface{} {
	out := make(map[string]interface{})
	out["type"] = "note"
	out["title"] = this.Title
	out["url"] = this.Url
	out["body"] = this.Body
	return out
}
