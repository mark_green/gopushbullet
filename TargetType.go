package gopushbullet

type TargetType interface {
	requestData
	GetTargetTypeName() string
}

type Device struct {
	DeviceId string	
}
func (this Device) GetTargetTypeName() string { return "Device" }
func (this Device) GetData() map[string]interface{} {
	out := make(map[string]interface{})
	out["device_iden"] = this.DeviceId
	return out
}

type Email struct {
	Email string	
}
func (this Email) GetTargetTypeName() string { return "Email" }
func (this Email) GetData() map[string]interface{} {
	out := make(map[string]interface{})
	out["email"] = this.Email
	return out
}
