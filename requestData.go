package gopushbullet

type requestData interface {
	GetData() map[string]interface{}
}

type unioned struct {
	data map[string]interface{}
}
func (this unioned) GetData() map[string]interface{} {
	return this.data
}
func union(dataSegments... requestData) requestData {
	data := make(map[string]interface{})
	for _, dataSegment := range dataSegments {
		for key, value := range dataSegment.GetData() {
			data[key] = value
		}
	}
	return unioned { data }
}